package org.raml.codegen

import org.apache.commons.io.FileUtils
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputFiles
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.TaskAction
import org.jsonschema2pojo.AnnotationStyle
import org.raml.RamlExtension
import org.raml.jaxrs.codegen.core.Configuration
import org.raml.jaxrs.codegen.core.Configuration.JaxrsVersion
import org.raml.jaxrs.codegen.core.Generator

class CodeGeneratorTask extends DefaultTask {

    Generator generator = new Generator()

    RamlExtension configuration

    @Input
    String getBasePackageName() {
        configuration.getBasePackageName()
    }

    @InputFiles
    Collection<File> getRamlFiles() {
        configuration.getRamlFiles()
    }

    @Input
    File getSourceDirectory() {
        configuration.getSourceDirectory();
    }

    @OutputDirectory
    File getOutputDirectory() {
        configuration.getOutputDirectory()
    }

    @TaskAction
    void generate() {
        Configuration ramlConfiguration = new Configuration()
        ramlConfiguration.setBasePackageName(getBasePackageName())
        ramlConfiguration.setOutputDirectory(getOutputDirectory())
        ramlConfiguration.setSourceDirectory(getSourceDirectory())
        ramlConfiguration.setJaxrsVersion(JaxrsVersion.JAXRS_2_0)
        ramlConfiguration.setJsonMapper(AnnotationStyle.JACKSON2)
        ramlConfiguration.setUseJsr303Annotations(true)

        FileUtils.cleanDirectory(configuration.getOutputDirectory());

        getRamlFiles().each { configurationFile ->
            generator.run(new FileReader(configurationFile), ramlConfiguration)
        }
    }

}
